FROM node:18-alpine3.15
COPY package*.json ./
RUN npm install
COPY . .
EXPOSE 3000
CMD ["node","src/index.js"]
