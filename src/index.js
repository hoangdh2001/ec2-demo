// build server
const express = require("express");

const app = express();
const server = require("http").Server(app);
const io = require("socket.io")(server);
//resources
app.use(express.static("./src/resources/static"));
app.set("view engine", "ejs");
app.set("views", "./src/resources/views");

// tạo kết nối giữa client và server
io.on("connection", function (socket) {
  console.log("connect:" + socket.id);
  socket.on("disconnect", function () {
    console.log("disconnect:" + socket.id);
  });
  //server lắng nghe dữ liệu từ client
  socket.on("client-send-message", function (data) {
    console.log(data);
    io.sockets.emit("server-send-message", { data, id: socket.id });
  });
});

// create route, display view

app.get("/", function (req, res) {
  res.render("index");
});

server.listen(3000, () => {
  console.log("connected to port 3000");
});
